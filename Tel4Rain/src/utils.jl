round4(x) = round(x, digits=4)

#maybe_length(itr) = _maybe_length(itr,Base.IteratorSize(itr))
#_maybe_length(itr,isz::Base.HasLength) = length(itr)
#_maybe_length(itr,isz::Base.HasShape) = length(itr)
#_maybe_length(itr,isz::Base.SizeUnknown) = missing
#slow_length(itr) = foldl( (x,_) -> x+1, itr ; init=0)

struct FlattenGenerator{I}
    it::I
end

""" `flatten_generator` is a specialized version of `flatten` that
implements length provided that the individual iterators know the
length """
flatten_generator(itr)=FlattenGenerator(Iterators.flatten(itr))
iterate(i::FlattenGenerator)=iterate(i.it)
iterate(i::FlattenGenerator,state)=iterate(i.it,state)
IteratorSize(i::FlattenGenerator)=HasLength()
IteratorEltype(i::FlattenGenerator)=HasEltype()
eltype(i::FlattenGenerator)=eltype(i.it)
length(i::FlattenGenerator)=sum(length,i.it)
