module Tel4Rain

import Base: iterate, eltype, length, IteratorSize, IteratorEltype
#import Base.iterator
include("utils.jl")
export flatten_generator, FlattenGenerator

export getdata, getdata_all_channels
include("raindata.jl")


export Args, build_model, get_small_data_loader_makers, get_big_data_loader_makers
export eval_loss_accuracy, loss, train, f1_loss, ce_loss, mcc_loss
include("rainflux.jl")





end # module
