using Serialization
import Feather
import CSV
import Random
import DataFrames

const  rainfile="rain.feather"
const interval=900 # interval length in seconds = 15min




time_to_intnum(t)=Int(ceil(t/interval))

""" Does it rain at time `t`? """
function rainsz(r,t) # `r` is the processed rain gauage data
   intn=time_to_intnum(t)
   rng=searchsorted(r.intn,intn)
   if length(rng)>0
       return r.rain[first(rng)]
   else
       return 0
   end
end


""" From preprocessed data, extract channel `id` in interval between times `t0` and `t1`, resampled to one minute intervals and add rain information from the gauges"""
function getdata(t0,t1,id;filename="../links.jsl")
  @info "Reading channel $(id) from $(filename)"  
  f=open(filename,"r") # read already parsed link data
  l2=deserialize(f)
  cc=l2[id]
  indc=(cc.time .>= t0) .& (cc.time .<= t1)
  c=cc[indc,:]
  drain=Feather.read(rainfile)
  indd=(drain.intn .>= time_to_intnum(t0) ) .& (drain.intn .<= time_to_intnum(t1))
  drain=drain[indd,:]
  c.y=map(t -> convert(Int8,rainsz(drain,t)),c.time)
  return c	
end

""" `getdata_all_channels` returns a dictionary, with channel labels as keys `id`, each value
    being the same as returned by `getdata(t0,t1,id;filename)` """
function getdata_all_channels(t0,t1;filename="../links.jsl")
    @info "Reading all channels from $(filename)"  
    f=open(filename,"r") # read already parsed link data
    l2=deserialize(f)
    drain=Feather.read(rainfile)
    indd=(drain.intn .>= time_to_intnum(t0) ) .& (drain.intn .< time_to_intnum(t1))
    drain=drain[indd,:]
    function process_channel(cc::DataFrames.DataFrame) # `cc` extracted channel data l2[id]
        indc=(cc.time .>= t0) .& (cc.time .<= t1)
        c=cc[indc,:] # restrict in time
        c.y=map(t -> convert(Int8,rainsz(drain,t)),c.time) # add rain information
        return c
    end
    return Dict( ( id => process_channel(c) ) for (id,c) in l2 )
end

""" Given a vector of rain labels, return indices suitable for learning which will be interpreted as end indices of `chunklen` long intervals. The dry periods (label -1) will be subsampled so that their number is approximately `beta`*number of dry labels.""" 
function getinds(y;args)
	indsrain=filter(t -> t >= args.chunklen, findall(y .== 1))
	indsdry=filter(t -> t >= args.chunklen, findall(y .== -1))
	ndry0=length(indsdry)
        nrain=length(indsrain)
	ndry=min(ndry0,round(Int,args.beta*nrain))
        @info "getinds: nrain=$(nrain) ndry0=$(ndry0) ndry=$(ndry)"
	indsdry=Random.shuffle(indsdry)[1:ndry]
	return Random.shuffle(vcat(indsdry,indsrain))
end
