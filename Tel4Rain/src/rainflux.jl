using Flux.Data: DataLoader
using Flux
using Parameters
import Dates
using DataFrames
using Base.Iterators: partition, repeated, take, drop
import ThreadsX
using Transducers
import BSON
import CUDA
using TimerOutputs: @timeit, TimerOutput
using ProgressMeter: @showprogress
using Logging
using LoggingExtras
import Statistics
using LinearAlgebra: dot

""" Structure Args contains all parameters for the Flux training """
@with_kw mutable struct Args
    lr0::Float64 = 1e-3   # initial learning rate
    lrmin::Float64 = 1e-7 # final learning rate
    epochs::Int = 1000
    #trainratio::Float64=0.8 # how much of the data to be used for training
    seed::Int = 100
    usecuda = false
    batchsize = 4
    infotime=1 # inform every that many epochs
    checkpointtime=100 # save model every that many epochs or zero for no checkpointing
    lambda=0.
    weight=20. # how much more weight to give to rainy periods    
    nstates=10 # state-space dimension
    maxnoimprovement=10 
    beta=1.0 # ratio of negative to positive samples
    #chunklen=2*24*60 # we will learn on sequences of this length (2 days)
    chunklen=4*60 # we will learn on sequences of this length (4 hours)
    #chunklen=40 # we will learn on sequences of this length (1 hour)
    burnin=5 # ignore this many initial samples
    savepath="runs/"
    #loadmodel=nothing
    #loadmodel="runs/model20210420.bson" # or nothing if learning should start from scratch
    #loadmodel="runs/model20210504a.bson" # or nothing if learning should start from scratch
    loadmodel="runs/model20210504d.bson"
    parallel=true
    logfile="runs/rainlog.txt" # if not nothing, log to that file in `train`
end

""" Given a vector `y` of length `n` , return a matrix `2 x n` where column
    `i` corresponds to y[i], encoding the with values -1, 0, 1 as [1 0], [0 0], and [0 1], respectively """
function pmone2onehot(y::Vector)
    [ 1f0*( y .< 0 )' ; 1f0*( y .> 0 )' ]
end

function pmone2onehot(y::T) where T<:Number
    z=zero(T)
    y>z ? UInt8[ 0, 1 ] : ( y<z ? UInt8[ 1, 0 ] : UInt8[ 0, 0 ] )
end
        

"""Generate sequences of training and testing data. We get  a sequence of batches. 
     Each batch itself is a sequence of pairs (x,y). `x` is a sequence of length `n` time points,
     each timepoint is a matrix `2 x m`, where `m` is the batch size. 
     `y` is a sequence of vectors of `2 x m` labels [1 0,0 0,0 1] where `[0 0]` means "don't care" """
function make_loader(data::DataFrame,inds;args=Args())
    # sequence of subarrays ending at indices `inds`
    X=( data.a[i-args.chunklen+1:i] for i in inds )
    Xbatched=( map(b -> reshape(b,(1,:)),Flux.batchseq(x,NaN32)) 
		         for x in partition(X,args.batchsize) )
    Y=( pmone2onehot.(data.y[i-args.chunklen+1:i]) for i in inds )
    Ybatched=( Flux.batchseq(y) for y in partition(Y,args.batchsize) )
    return zip(Xbatched,Ybatched)
end

""" `make_loader` method for a dictionary of DataFrames for all channels, returned from `getdata_all_channels` """
function make_loader(d::Dict,inds;args=Args())
    # sequence of subarrays ending at indices `inds` for all channels
    function do_channel(data)
      indf=filter( i -> i-args.chunklen >= 0 && i<= DataFrames.nrow(data), inds)
      X=( data.a[i-args.chunklen+1:i] for i in indf)
      Xbatched=( map(b -> reshape(b,(1,:)),Flux.batchseq(x,NaN32)) 
		         for x in partition(X,args.batchsize) )
      Y=( pmone2onehot.(data.y[i-args.chunklen+1:i]) for i in indf )
      Ybatched=( Flux.batchseq(y,NaN32) for y in partition(Y,args.batchsize) )
      return zip(Xbatched,Ybatched)
    end
    @info "make_loader: $(length(keys(d))) channels and $(length(inds)) indices per channel before prunning."
    
    #return Iterators.flatten( do_channel(d[id]) for id in keys(d) )
    return flatten_generator( do_channel(d[id]) for id in keys(d) )
end



""" `get_small_data_loader_makers` returns two functions, `(make_train_loader,
    make_test_loader)` which should be called at the beginning of each
    epoch to produce `train_loader` and `test_loader` sequences of
    batches.  """
function get_small_data_loader_makers(args)
    @info "Creating small data loaders"
    """Let us read the attenuation of one of the radio channels in one minute intervals for a defined time interval."""
    t0=Dates.datetime2unix(Dates.DateTime(2016,6,1)) # initial date
    t1=Dates.datetime2unix(Dates.DateTime(2016,10,1)) # final date
    id=56 # channel number 
    traindata=getdata(t0,t1,id)
    testdata=getdata(Dates.datetime2unix(Dates.DateTime(2015,6,1)),
	Dates.datetime2unix(Dates.DateTime(2015,10,1)),id)
    function make_train_loader()
        training_inds=getinds(traindata.y,args=args)
        make_loader(traindata,training_inds;args=args)
    end
    function make_test_loader()
        testing_inds=getinds(testdata.y,args=args)
        make_loader(testdata,testing_inds;args=args)
    end
    return make_train_loader, make_test_loader
end

function get_big_data_loader_makers(args)
    @info "Creating big data loaders"
    """Let us read the attenuation of all radio channels in one minute intervals for a defined time interval."""
    t0=Dates.datetime2unix(Dates.DateTime(1900,1,1)) # initial date for the training data
    t1=Dates.datetime2unix(Dates.DateTime(2015,12,31)) # final date training data
    t2=Dates.datetime2unix(Dates.DateTime(2999,12,31)) # final date for testing data
    traindata=getdata_all_channels(t0,t1)
    testdata=getdata_all_channels(t1,t2)
    channel=first(keys(traindata))
    function make_train_loader()
        training_inds=getinds(traindata[channel].y,args=args)
        make_loader(traindata,training_inds;args=args)
    end
    function make_test_loader()
        testing_inds=getinds(testdata[channel].y,args=args)
        make_loader(testdata,testing_inds;args=args)
    end
    return make_train_loader, make_test_loader
end


""" evaluate logitcrossentropy for one time point of a batch given the softmax inputs
    `outpb` and the reference labels `yb` 
"""
# function logitcrossentropy0(outpb,yb;weight=1.0)
#     m=size(yb,1) # yb is a vector of labels
#     @assert (size(outpb,1)==2) # matrix `2 x m`  of softmax inputs
#     @assert (size(outpb,2)==m)
#     epsilon=eps(Float32)
#     logsumexp=log.(sum(exp.(outpb),dims=1).+epsilon)
#     l=0.
#     for j=1:m     # TODO parallelize if possible
#       if yb[j]==-1
#          l -= outpb[1,j] - logsumexp[j] 
#       elseif yb[j]==1
#          l -= weight .* ( outpb[2,j] - logsumexp[j] )
#       end
#     end
#     return l
# end

function weighted_logitcrossentropy(outp,y;weight=1.0)
    Statistics.mean(-sum(y .* Float32[ weight, 1f0 ] .* logsoftmax(outp)))
end


""" Criterion function given a batch,  composed of `m` sequences,
    represented by a sequence of length `n` of an array of `(2,m)` softmax inputs `x`
    and a sequence of length `n` of matrices of `(2,m)` one-hot encoded labels (or all zeros), evaluate the cross-entropy.
    Returns the sum of all time points and mean over all sequences in the batch."""
function ce_loss(outp,y;args=Args()) 
	return sum(drop(weighted_logitcrossentropy.(outp,y;weight=args.weight),args.burnin)) # sum over time
end

""" Given network output `outp` (size numclasses x batchsize) and maybeonehot labels `y` (size numclasses x batchsize), evaluate the
    complement softF1 criterion """
function softF1(outp,y)
    @assert (size(outp,1)==2)
    @assert (size(y,1)==2)
    n=size(outp,2)
    @assert (size(y,2)==n)
    p=Flux.softmax(outp) # size numclasses x batchsize of probabilities
    tp=dot(p[2,:],y[2,:]) # number of true positives (soft)
    pos=sum(y[2,:])       # number of positive samples
    sel=dot(p[2,:],dropdims(sum(Float32.(y[:,:]),dims=1),dims=1)) # number of selected samples
    #sel=sum( p[2,:] .* sum(y,dims=1)
    #sel=(sum(y,dims=1) * p[2,:])[1]
    den=pos+sel # denominator
    1.0f0 - ( 2f0 * tp / den ) # 1. - f1
end

""" Criterion based on the soft F1 function, same arguments as `ce_loss` """
function f1_loss(outp,y;args=Args())
    sum(drop(softF1.(outp,y),args.burnin)) # sum over time
    #sum(softF1.(outp,y;args=args)) # sum over time
end

""" Matthews correlation coefficient for network output `outp` (size numclasses x batchsize)
    and maybeonehot labels `y` (size numclasses x batchsize) """
function mcc(outp,y;args=Args())
    @assert (size(outp,1)==2)
    @assert (size(y,1)==2)
    n=size(outp,2)
    @assert (size(y,2)==n)
    @assert all( (y .== 0) .| (y .== 1))  # check validity of 'y'
    @assert all(sum(y,dims=1) .<= 1)
    p=Flux.softmax(outp) # size numclasses x batchsize of probabilities
    c=y*p' # confusion matrix
    nom=c[2,2]*c[1,1]-c[1,2]*c[2,1] # tp*tn-fp*fn
    pos=c[2,1]+c[2,2] # number of positive samples
    sel=c[1,2]+c[2,2] # number of selected
    nclass=sum(c) # number of classified samples
    if (nclass < sel) || ( nclass < pos )
        print("c=",c)
        @assert false
    end
    small_number=1.
    den=sqrt(sel*pos*(nclass-sel)*(nclass-pos)+small_number)
    nom/den
end
    
""" negative of the Matthews correlation coefficient criterion for length `n` sequences,
same arguments as `ce_loss` """
function mcc_loss(outp,y;args=Args())
    - Statistics.mean(drop(mcc.(outp,y;args=args),args.burnin)) # mean over the time in sequence
end



# """ Evaluate the classification accuracy, inputs as in `loss`. Returns also the number of sequences = number of minibatches * sequence length """
# function numcorr0(x,y)
#     m=size(y,1) # chunk size
#     @assert (size(x,1)==2)
#     @assert (size(x,2)==m)	
# 	rx=Flux.onecold(x)
# 	return sum( ( rx .== ( y .+ 3 ) ./ 2 ) .& ( y .!= 0 ))
# end


""" Convert softmax inputs to classes -1 or 1 """
function outp2class(outp)
	assert(length(outp)==2)
	return ifelse(outp[1]>outp[2],-1,1)
end

""" Convert softmax inputs to classes 1 or 2 """
function outp2classind(outp)
	@assert(length(outp)==2)
	return ifelse(outp[1]>outp[2],1,2)
end

""" convert -1,1 class encoding to 1,2 """
function classind(x)
	return ifelse(x>0,2,1)
end


""" Let us evaluate all possible binary classification outcomes by calculating the confusion matrix given one minibatch outputs and reference classification (-1,0,1) or one hot encoding"""
function confmat0(outp,y)
  m=size(y,2)
  @assert (size(outp,2)==m)	
  cm=zeros(Int,(2,2)) # confussion matrix
  if size(outp,1)==1  
      @assert (size(y,1)==1)  
      for i=1:m
          if y[i]!=0
	      cm[classind(y[i]),outp2classind(outp[:,i])]+=1			
          end
      end # for i
  else # one hot encoding
      @assert (size(outp,1)==2)
      @assert (size(y,1)==2)  
      for i=1:m
          if sum(y[:,i])>0
	      cm[outp2classind(y[:,i]),outp2classind(outp[:,i])]+=1			
          end
      end # for i

  end
  return cm	
end

""" Calculate a confusion matrix given a sequence of minibatches """
function confmat(outps,ys)
	return sum(confmat0.(outps,ys))
end

function sum_pair((a,b),(c,d))
    (a+c,b+d)
end

""" Do model evaluation on a dataset, return a named tuple with measures such as accuracy, precision, recall...  """
function eval_loss_accuracy(loader, model, device,loss_function, args)
    function process_batch((x,y)) 
        m=deepcopy(model) |> device # make a local copy of the model
        x, y = x |> device, y |> device
	Flux.reset!(m) # reset the sequence model
        outp = m.(x) # apply the model 
	lv=loss_function(outp,y,args=args)  
	cm=confmat(outp |> cpu, y |> cpu)
        cm,lv
    end
    cm=zeros(Int,(2,2))
    l = 0. # loss function
    if args.parallel
        cm,l=foldxl(sum_pair, loader |> NondeterministicThreading() |> Map(process_batch))
    else # single thread
        #@info "eval_loss_accuracy, single thread"        
        cm,l=foldl(sum_pair, loader |>  Map(process_batch))
    end
    ntot=sum(cm)
    corrs=cm[2,2]+cm[1,1] # correct ones
    precision = cm[2,2]/(cm[2,2]+cm[1,2])
    recall = cm[2,2]/(cm[2,2]+cm[2,1])
    f1=2.0 / ( 1. / recall + 1. / precision)
    return (loss = l/ntot |> round4, acc = corrs/ntot*100 |> round4,
		    tp=cm[2,2], fp=cm[1,2], tn=cm[1,1], fn=cm[2,1],
	        recall = recall*100 |> round4,
	    precision = precision*100 |> round4,
            specificity = cm[1,1] / (cm[1,1]+cm[1,2]) * 100 |> round4,
            f1=f1*100 |> round4)
end


""" build a recurrent model with N dimensional internal state """
function build_model(args)
    N=args.nstates
    return Chain(GRU(1,N),GRU(N,N),Dense(N,2))
end

num_params(model) = sum(length, Flux.params(model)) 

""" Default implementation of the reporting function. Needs to return the validation loss """
function report_default(epoch,train_loader,test_loader,model,device,loss_function, args)
        train = eval_loss_accuracy(train_loader, model, device, loss_function, args)
        test = eval_loss_accuracy(test_loader, model, device, loss_function, args)        
        @info "Epoch: $epoch   Train: $(train)   Test: $(test)"
        return test.loss
end

""" `g += h * scale` for `Grads` """
function add_scaled_gradient!(g,h,scale)
  psg=g.params
  psh=h.params # components may not have the same order
  for j=1:length(psg)
      # some parts of the gradients may not be evaluated   
      (isnothing(h[psh[j]]) || isnothing(g[psg[j]])) && continue      
      g[psg[j]] .+= h[psh[j]] .* scale
  end
end

""" evaluate the gradient """
function eval_gradient(m,(x,y),device,loss_function,args)
    x, y = x |> device, y |> device
    Flux.reset!(m)
    ps=Flux.params(m)
    Flux.gradient(ps) do
      ŷ = m.(x) # apply to a sequence
      loss_function(ŷ, y,args=args)
      end
end

""" parallel gradient calculation like `eval_gradient` but `batch` is a sequence of (x,y) pairs 

    TODO: accelerate by preallocating """
function eval_gradient_parallel(model,batch,device,loss_function,args)
    Flux.reset!(model)
    n=length(batch)
    models= [ i==1 ? model : deepcopy(model) for i=1:n ]
    gss = ThreadsX.map((m,xy) -> eval_gradient(m,xy,device,loss_function,args), models,batch)
    # calculate the mean gradient by aggregating to the first gradient belonging to `model`
    gss[1] .*= 1 / n
    for i=2:n
        add_scaled_gradient!(gss[1],gss[i],1/n)
    end
    return gss[1]
end




""" Returns a trained model """
function train(train_loader_maker,test_loader_maker,model,device;
               loss_function=ce_loss, report_function=report_default,args=Args())
    #args = Args(; kws...)

    (args.checkpointtime % args.infotime != 0) && error("args.checkpointtime must be a multiple of args.infotime")

    if !isnothing(args.logfile)
        @info "Starting to log to $(args.logfile)"
        logger=TeeLogger(
            MinLevelLogger(FileLogger(args.logfile,append=true),Logging.Debug),
            ConsoleLogger(stdout,Logging.Debug))
        #logf=open(args.logfile,"w+")
        #logger=SimpleLogger(logf)
        global_logger(logger)
    end
    
    to=TimerOutput()

    args.seed > 0 && Random.seed!(args.seed)
    usecuda = args.usecuda && CUDA.functional()
    
    if usecuda
        device = gpu
        @info "Training on GPU"
    else
        device = cpu
        @info "Training on CPU"
    end

        
    
    ## MODEL AND OPTIMIZER
    #model = build_model(args) 
    if !isnothing(args.loadmodel)
    #    model = build_model(args.nstates)
    #else
        @info "Loading model from $(args.loadmodel)" 
        #BSON.@load args.loadmodel model
        model=BSON.load(args.loadmodel, @__MODULE__)[:model]
    end
    @info "GRU model: $(num_params(model)) trainable params"    
    @info "batch size: $(args.batchsize)"
    bestmodel=deepcopy(model) # here to store the best model so far
    model=model |> device    

    opt = ADAM(args.lr0) 
    if args.lambda > 0 # add weight decay, equivalent to L2 regularization
        opt = Optimiser(opt, WeightDecay(args.λ))
    end
    
    
    ## TRAINING
    if args.parallel
        @info "Start parallel training"
    else
        @info "Start single thread training"
    end

    # test data is prepared once and for all
    test_loader = test_loader_maker()
    train_loader=train_loader_maker()
    @info "Dataset Rain: $(length(train_loader)) train and $(length(test_loader)) test batches. Sequence length $(length(first(first(train_loader)))), burn-in $(args.burnin)"
    report(epoch) = report_function(epoch,train_loader,test_loader,model,device,loss_function,args)
    bestloss=report(0) # best test loss so far
    prevloss=bestloss # previous iteration
    noimprovement=0 # no improvement for that many epochs    
    nthreads=Threads.nthreads() # number of threads


    
    for epoch in 1:args.epochs
        @info "Starting training epoch $epoch, lr=$(opt.eta)"
        ## DATA
        @timeit to "loader" train_loader=train_loader_maker() # fresh training data

        # (!isnothing(args.logfile) && flush(logf))
        
        @timeit to "gradient" begin 
        if args.parallel
             @showprogress for b in partition(train_loader,nthreads)
                 gs=eval_gradient_parallel(model,b,device,loss_function,args)
                 ps=Flux.params(model)
                 Flux.Optimise.update!(opt, ps, gs)
             end # for b (iterate over batches)
         else # serial training    
            @showprogress for xy in train_loader
                #@info "started batch $(length(x)) $(size(first(x))) $(size(first(y)))"
                gs = eval_gradient(model,xy,device,loss_function,args)
                ps=Flux.params(model)
                Flux.Optimise.update!(opt, ps, gs)
            end # for (x,y) in one epoch
         end # if args.parallel
        end

        
        ## Printing and logging
        if (epoch % args.infotime == 0)
            @timeit to "report" lossval=report(epoch)
            ## checkpointing
            if (args.checkpointtime>0 && epoch % args.checkpointtime == 0)
                !ispath(args.savepath) && mkpath(args.savepath)
                modelpath = joinpath(args.savepath, "checkpoint-epoch$(epoch)-loss$(round(lossval,digits=4))-$(Dates.now())-model.bson") 
                @info "Model saved to \"$(modelpath)\""
                BSON.bson(modelpath, model = cpu(model))
            end
            if lossval < bestloss # test loss improved
                @info "Best model updated epoch=$(epoch) loss=$(lossval)"
                noimprovement=0 # reset the counter
                bestloss=lossval
                bestmodel=deepcopy(model)
            else
                noimprovement += args.infotime
                if noimprovement >= args.maxnoimprovement
                    opt.eta *= 0.1 # learning rate reduction
                    model=deepcopy(bestmodel) # start with the best model so far
                    if opt.eta <= args.lrmin
                        @info "No improvement in $noimprovement iterations, interrupting."
                        break
                    end
                    @info "No improvement in $noimprovement iterations, reducing learning rate."
                    noimprovement=0
                end # if notimprovement
            end # if lossval
        end # if epoch
    end # for epochs
    @info "Training finished."
    print(to) # print timing
    !ispath(args.savepath) && mkpath(args.savepath)
    modelpath = joinpath(args.savepath, "model.bson") 
    bestmodel=cpu(bestmodel) #return model to cpu before serialization
    BSON.bson(modelpath, model = bestmodel)
    @info "Model saved in \"$(modelpath)\""
    return bestmodel
end # function train

