# Classification of the iris dataset using Flux
#
# Jan Kybic, 2021

using Flux
using RDatasets
using DataFrames
using Random
using Plots
using Statistics
using Parameters
using ProgressMeter
using CUDA
using TimerOutputs
using Flux.Data: DataLoader
using Flux.Losses: logitcrossentropy
using Flux: onehotbatch, onecold

@with_kw mutable struct Args
    lr::Float64 = 0.1
    epochs::Int = 1000
    trainratio::Float64=0.8 # how much of the data to be used for training
    seed::Int = 100
    usecuda = true
    batchsize = 10
    infotime=10
    lambda=0.
    maxnoimprovement=40    
end


""" build the model """
simple_model() = Chain(Dense(3,100,relu),Dense(100,100,relu),Dense(100,3))


""" given a categorical array, convert to onehot encoding """
function onehot_from_categorical(s)
    l=levels(s) ; n=length(s) ; m=length(l)
    y=zeros(Int8,(n,m))
    for i=1:n
        for j=1:m
            y[i,j]= s[i]==l[j]
        end
    end
    return y
end

function normalize(X)
    stats=(meanX=mean(X,dims=2), stdX=std(X,dims=2))
    return (normalize(X,stats),stats)
end

function normalize(X,stats)
    return  (X .- stats.meanX) ./ stats.stdX
end
             
""" split data to training and testing """
function split_to_train_test(X,Y,args)
    n=size(X,1)
    i_rand=randperm(n)
    n_train=round(Int,n*args.trainratio) # size of the training set
    i_train=i_rand[1:n_train] ; i_test=i_rand[n_train+1:n]
    Xtrain=X[i_train,:]' ; Ytrain=Y[i_train,:]'
    Xtest=X[i_test,:]' ; Ytest=Y[i_test,:]'
    return ((Xtrain,Ytrain),(Xtest,Ytest))
end

""" return training and testing data as (X,Y) tuples, where Y uses one hot encoding"""
function get_data(args)
    iris=dataset("datasets","iris")
    X=convert(Matrix,iris[:,2:4]) # features
    Y=onehot_from_categorical(iris.Species)
    ((Xtrain,Ytrain),(Xtest,Ytest))=split_to_train_test(X,Y,args)
    Xtrain,Xstats=normalize(Xtrain)
    Xtest=normalize(Xtest,Xstats)
    train_loader = DataLoader((Xtrain, Ytrain), batchsize=args.batchsize, shuffle=true, partial=true)
    test_loader = DataLoader((Xtest, Ytest),  batchsize=args.batchsize, partial=true)
    return train_loader, test_loader
end

loss(ŷ, y) = logitcrossentropy(ŷ, y)

 ## utility functions
num_params(model) = sum(length, Flux.params(model)) 
round4(x) = round(x, digits=4)

function eval_loss_accuracy(loader, model, device)
    l = 0f0
    acc = 0
    ntot = 0
    for (x, y) in loader
        x, y = x |> device, y |> device
        ŷ = model(x)
        l += loss(ŷ, y) * size(x)[end]        
        acc += sum(onecold(ŷ |> cpu) .== onecold(y |> cpu))
        ntot += size(x)[end]
    end
    return (loss = l/ntot |> round4, acc = acc/ntot*100 |> round4)
end
    
function train(;kws...)
    args=Args(;kws...)
    to=TimerOutput()
    
    args.seed > 0 && Random.seed!(args.seed)
    use_cuda = args.usecuda && CUDA.functional()
    
    if use_cuda
        device = gpu
        @info "Training on GPU"
    else
        device = cpu
        @info "Training on CPU"
    end

    train_loader, test_loader = get_data(args)
    @info "Dataset IRIS: $(train_loader.nobs) train and $(test_loader.nobs) test examples"

    model = simple_model() |> device
    @info "$(num_params(model)) trainable params"

    ps = Flux.params(model)  

    opt = ADAM(args.lr)
    #opt = Descent(args.lr)
    if args.lambda > 0 # add weight decay, equivalent to L2 regularization
        opt = Optimiser(opt, WeightDecay(args.λ))
    end

    function report(epoch)
        train = eval_loss_accuracy(train_loader, model, device)
        test = eval_loss_accuracy(test_loader, model, device)        
        println("Epoch: $epoch   Train: $(train)   Test: $(test)")
        #if args.tblogger
        #    set_step!(tblogger, epoch)
        #    with_logger(tblogger) do
        #        @info "train" loss=train.loss  acc=train.acc
        #        @info "test"  loss=test.loss   acc=test.acc
        #    end
        #end
        return test.acc
    end

    @info "Start training"
    report(0)
    prevacc=0. # previous accuracy
    noimprovement=0 # no improvement for that many epochs    
    @timeit to "training"  for epoch in 1:args.epochs
        #@showprogress
        for (x, y) in train_loader
             x, y = x |> device, y |> device
            gs = Flux.gradient(ps) do
                    ŷ = model(x)
                    loss(ŷ, y)
            end

            Flux.Optimise.update!(opt, ps, gs)
        end # for (x,y)
        ## Printing and logging
        if (epoch % args.infotime == 0)
            acc=report(epoch)
            if acc>prevacc
                noimprovement=0 # reset the counter
            else
                noimprovement += args.infotime
                if noimprovement >= args.maxnoimprovement
                    @info "No improvement in $noimprovement iterations, interrupting."
                    break
                end
            end
            prevacc=acc
        end
        #if args.checktime > 0 && epoch % args.checktime == 0
        #    !ispath(args.savepath) && mkpath(args.savepath)
        #    modelpath = joinpath(args.savepath, "model.bson") 
        #    let model = cpu(model) #return model to cpu before serialization
        #        BSON.@save modelpath model epoch
        #    end
        #    @info "Model saved in \"$(modelpath)\""
        #end
    end # for epoch
    print(to)    
end # end function train
    

train()
