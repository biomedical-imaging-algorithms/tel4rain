"""
Functions for processing data from the tel4rain project -
estimating rain from microwave link attenuation data
"""

module tel4rain

#import CSVFiles
import CSV
using DataFrames
import Dates
import Feather
using Logging
using Statistics
using Glob
using Serialization
using Plots
using Optim
using MLBase
using MLDataPattern
using Printf
using LIBSVM

Logging.disable_logging(Logging.BelowMinLevel)

export raingauges_to_feather, rains, process_gauges, process_all_links
export optimizer_threshold_f1, tempfile, time_to_intnum, featherfile
const  prefix="/mnt/medical/tel4rain"
const  featherfile="raingauges.feather"
const  tempfile="../temperature_Prosek.csv"
const  rainfile="rain.feather"
const interval=900 # interval length in seconds = 15min
#const interval=300 # interval length in seconds = 5min

""" read the CSV file with rain gauge information and save it to the feather file """
function raingauges_to_feather()
  raingaugesfile=joinpath(prefix,"rain_gauges/rain_gauges.csv")
  println("Converting rain gauges file $raingaugesfile ...")  
    #df=DataFrames.DataFrame(CSVFiles.load(raingaugesfile;delim=';'))
  df=DataFrames.DataFrame(CSV.File(raingaugesfile;delim=';'))
  Feather.write(featherfile,df)
  println("   written to $featherfile.")  
end


""" given a vector of vectors of rain gauge measurements, output a label (-1,0,1) whether it rains or not """
function rains(v)
    @assert( length(v) > 0 )
    if length(v[1])<5
        return missing # time series too short to evaluate
    end
    vmin=minimum(map(Statistics.mean,v))
    if ismissing(vmin) return missing end
    if vmin>0.5 return 1 end # rains everywhere 
    if maximum(map(maximum,v))<0.1 return -1 end # dry everywhere        
    return missing
end

time_to_intnum(t)=Int(ceil(t/interval))

""" process the data from the rain gauges and temperature, producing a reference mapping whether it rains in a certain interval. save it to file """
function process_gauges()
  @info "Reading $featherfile"
  @time df=Feather.read(featherfile)
  df[!,:intn]=map(time_to_intnum,df.time) # add interval number as a column
  @info "Combining by intervals"
  #drain=DataFrames.combine(DataFrames.groupby(df,:intn),[:D10,:D13,:D22] => (x -> rains([x...])) => :rain )
  #rename!(drain,:x1 => :rain) # TODO - do it in one step  
  #drain=DataFrames.combine(DataFrames.groupby(df,:intn),r -> (rain=rains([r.D10,r.D13,r.D22])))  
  drain=DataFrames.combine(r -> (rain=rains([r.D10,r.D13,r.D22]),),DataFrames.groupby(df,:intn))  
  dropmissing!(drain)
  @info "Reading  $tempfile"
  dtemp=DataFrames.DataFrame(CSV.File(tempfile;delim=';'))
  dtemp[!,:intn]=map(time_to_intnum,dtemp.time) # add interval number as a column
  select!(dtemp,Not(:time))
  @info "Combining by intervals"
  #dtemp=combine(groupby(dtemp,:intn),:temperature => minimum)
  #rename!(dtemp,:x1 => :mintemp) # TODO - do it in one step  
  dtemp=combine(r -> (mintemp=minimum(r.temperature),),groupby(dtemp,:intn))

  @info "Joining..."
  drain=innerjoin(drain,dtemp,on=:intn)
  @info "Filtering..."
  filter!(:mintemp => t -> t > 5,drain)
  select!(drain,Not(:mintemp))
  @info "Writing to $rainfile"  
  Feather.write(rainfile,drain)
  @info "Finished..."  
  return drain
end

pint(x)=parse(Int,x)

""" process CML metadata """
function process_cmlmeta()
  cmlmetadatafile=joinpath(tel4rain.prefix,"CML_data/cml_metadata.csv")
  @info "Reading  $cmlmetadatafile"
  cm=DataFrames.DataFrame(CSV.File(cmlmetadatafile;delim=';'))
  cm[!,:id1]=Array{Union{Missing,Int}}(missing,size(cm,1))
  cm[!,:id2]=Array{Union{Missing,Int}}(missing,size(cm,1))
  for r in eachrow(cm)
        try
           v=map(pint,split(r.id,"_"))
           r.id1=v[1]
           r.id2=v[2] 
        catch e
          println("Skipping ",r.id )
        end
  end
  dropmissing!(cm)
  return cm
end

""" given a channel id, find the length of the link length from the `cm` DataFrame provided by `process_cmlmeta` """
function link_length(cm,i)
    ind=findfirst((cm.id1 .== i) .| (cm.id2 .== i) )
    if isnothing(ind) return missing end
    return cm[ind,:length]
end




""" Read all files with link attenuation data and saves the process data. `cm` is the metadata calculated by 'process_cmlmeta' """
function process_all_links(cm)
  links=Dict{Int,Any}()
  files=glob("[0-9]*.csv",joinpath(tel4rain.prefix,"CML_data"))
  for fn in files
      m=match(r"([0-9]+)\.csv",fn)
      if isnothing(m)
          @info "Skipping file $fn"
          continue
      end
      linkid=parse(Int,m[1])    
      @info "Reading file $fn to link id=$linkid"
      ll=link_length(cm,linkid)/1000. # link length in meters
      if !ismissing(ll)
          links[linkid]=process_one_link(fn,ll)
      else
          @info "Link length information missing, skipping link file."
      end
  end

  #open("../links.jsl","w") do f
  #  @time serialize(f,links)
  #  close(f)
  return links
end

""" process one link. calculate attenuation in dB/km and mean attenuation by minutes """

function process_one_link(fn,ll)
  @info "Reading link file $fn"  
  c=DataFrame(CSV.File(fn;delim=';'))
  c[!,:a]=(c.txpower-c.rxpower)/ll # attenuation in dB/km
  c[!,:intn]=map(time_to_intnum,c.time)
  c[!,:minutes]=Int.(ceil.(c.time ./ 60.)) # time in minutes
  cc=combine(groupby(c,:minutes),:time => median => :time, :intn => ( v -> Int.(median(v)) ) => :intn, 
             :a => mean => :a)
  cc[!,:a]=Float32.(cc.a)
  cc=cc[!,[:time,:a]]
  return cc
end  
  
""" calculate baseline for a link dataframe with a given lag (in seconds) """
function calculate_baseline(dl,lag)
  b=Array{Union{Float32,Missing}}(undef,nrow(dl))  
  i0=1
  for i in 1:nrow(dl)
      t=dl[i,:time] # current time
      t0=t-lag    # start of the baseline calculation window
      while (dl.time[i0]<t0) # shift the bottom pointer
          i0+=1
          end
      if t-dl.time[i0]<0.9*lag
          b[i]=missing
      else
          b[i]=minimum(dl[i0:i,:a])
      end
  end
  return b      
end

""" Does it rain at time `t`?
`r` - dataframe of ground truth rain data calculated by `process_gauges`
"""

function rainsq(r,t)
   intn=time_to_intnum(t)
   rng=searchsorted(r.intn,intn)
   if length(rng)>0
       return r.rain[first(rng)]
   else
       return missing
   end
end

function rainsz(r,t)
   intn=time_to_intnum(t)
   rng=searchsorted(r.intn,intn)
   if length(rng)>0
       return r.rain[first(rng)]
   else
       return 0
   end
end

""" create a dataframe containing preprocessed features for selected link channels 

    dataframe structure: time channel y x0 x1 x2 x3 x4 

where xn is the mean of the attenuation during last lag_n minutes, after baseline subtraction
'y' is the ground truth label (if available)

    `l` - dictionnary of dataframes created by process_all_links
    `r` - dataframe of ground truth rain data calculated by `process_gauges` (optional)
    `channels` - list of channels to process, or `nothing`, meaning all channels
    `lags` - list of lags in minutes
    `b` - baseline length in seconds, defaults to 1 day
"""
function create_features(l,r=nothing;channels=nothing,lags=[1,2,5,10],b=24*3600)
  nfeatures=length(lags)  
  df=DataFrame()
  if !isnothing(r)
      @assert ( issorted(r.intn) )
  end
  for k in keys(l)
    if channels==nothing || k in channels
      @info "create_features: processing channel $k"  
      cc=copy(l[k]) # dataframe with the selected channel 
      base=calculate_baseline(cc,b)  
      cc.a=cc.a-base # subtract the baseline from the signal
      cc[!,:channel] .= k 
      for i in 1:length(lags)
          @info "    processing lag $(lags[i])"
          f=lagged_mean(cc,lags[i]*60) # calculate the averaged signal
          cc[!,Symbol('x' * repr(i))]=f
      end
      if !isnothing(r)
        cc[!,:y]=map(t->rainsq(r,t),cc.time)
      end
      dropmissing!(cc)  
      append!(df, cc)
    end
  end

  #@info "Saving to features.jsl"
  #open("../features.jsl","w") do fd
  #@time serialize(fd,f)
  #close(fd)

  return df        
end

""" given a dataset with fields :time and :lag, return a vector containing the mean of the signal :a between the current time `t` and `t-lag`. Both `t` and `lag` are in seconds. """
function lagged_mean(d,lag)
  n=nrow(d)
  b=Array{Union{Float32,Missing}}(undef,n)  
  i0=1
  for i in 1:n
      t=d[i,:time] # current time
      t0=t-lag    # start of the baseline calculation window
      while i0<n && (d.time[i0+1]<=t0) # shift the bottom pointer
          i0+=1
          end
      if t-d.time[i0]<0.9*lag
          b[i]=missing
      else
          b[i]=mean(d[i0:i,:a])
      end
  end
  return(b)
end

function print_eval_classifier(predicted,y)
  @show cm=confusmat(2, div.(y .+ 3,2), div.(predicted .+ 3, 2) )
  recall=cm[2,2]/(cm[2,2]+cm[2,1])*100
  prec=cm[2,2]/(cm[2,2]+cm[1,2])*100 
  fpr=cm[1,2]/(cm[1,1]+cm[2,1])*100
  f1=2. / (1. / prec + 1. / recall)  
  @printf "Accuracy: %.2f%% precision: %.2f%% recall: %.2f%% fpr: %.2f%% f1: %.2f%%\n" mean((predicted .== y))*100 prec recall fpr f1
end

""" Convert svmpredict values to -1,1 predictions """
function values2pred(values,thr)
      return ( values .< thr ) .*2 .- 1  
end

function eval_f1(outp,y,thresh)
  pred=values2pred(outp,thresh)  
  cm=confusmat(2, div.(y .+ 3,2), div.(pred .+ 3, 2) )
  recall=cm[2,2]/(cm[2,2]+cm[2,1])*100
  prec=cm[2,2]/(cm[2,2]+cm[1,2])*100 
  f1=2. / (1. / prec + 1. / recall)  
  return f1
end

function eval_accuracy(outp,y,thresh)
  return mean( (outp .< thresh) .== ( y .> 0 ) )
end


""" given the classifier output `outp` and the (+1,-1) ground truth `y`, find a threshold optimizing the classification accuracy """
function optimize_threshold_accuracy(outp,y)
   t0=minimum(outp)
   t1=maximum(outp)
   res=optimize(t -> - eval_accuracy(outp,y,t),t0,t1) # Brent method
   #print("Optimizer: ",res)
   return res.minimizer
end

function optimize_threshold_f1(outp,y)
   t0=minimum(outp)
   t1=maximum(outp)
   res=optimize(t -> - eval_f1(outp,y,t),t0,t1) # Brent method
   #print("Optimizer: ",res)
   return res.minimizer
end



# -------------------- test functions ---------------------------------

function test_linear_classification()
  f=deserialize(open("../features.jsl","r"))

  t0=Dates.datetime2unix(Dates.DateTime(2016,6,1))
  t1=Dates.datetime2unix(Dates.DateTime(2016,10,1)) 
  f=f[t0 .<= f.time .<= t1,:]
  y=f.y
  X=convert(Array,f[:,r"x[0-9]+"])'
  X=X[1:2,:]  
  (Xtrain, ytrain), (Xtest, ytest) = stratifiedobs((X, y), p = 0.7, shuffle=true);
  for c in [0.01,0.1,1.,10.,100.]  
      @info "Started training"
      @time model=svmtrain(Xtrain,ytrain,kernel=Kernel.Linear,verbose=false,shrinking=false,cost=c)
      @info "-------- C=$c --------"
      @time (predicted,values)=svmpredict(model,Xtest)
      values=values[1,:]
      tel4rain.print_eval_classifier(predicted,ytest)
      thr=tel4rain.optimize_threshold_accuracy(values,ytest)
      pred1=values2pred(values,thr)  
      acc=tel4rain.eval_accuracy(values,ytest,thr)
      acc0=tel4rain.eval_accuracy(values,ytest,0.)
      @info "Optimized threshold accuracy: $thr accuracy: $acc before: $acc0 "
      tel4rain.print_eval_classifier(pred1,ytest)
      thr2=tel4rain.optimize_threshold_f1(values,ytest)
      f1=tel4rain.eval_f1(values,ytest,thr2)
      f10=tel4rain.eval_f1(values,ytest,thr2)
      @info "Optimized threshold f1: $thr2 f1: $f1 before: $f10 "
      pred2=values2pred(values,thr2)  
      tel4rain.print_eval_classifier(pred2,ytest)
  end
end

"""  Compare attenuation from one CML,  rain estimatin provided and our binary rain classification from rain gauges """
function show_signal_and_rain(id=56)
  f=open("../links.jsl","r") # read already parsed link data
  @time l2=deserialize(f)
  cc=l2[id]                  # choose channel id
  #t0=Dates.datetime2unix(Dates.DateTime(2016,7,5)) # was 1
  #t1=Dates.datetime2unix(Dates.DateTime(2016,7,6)) # was 10
  t0=Dates.datetime2unix(Dates.DateTime(2016,5,1)) # was 1
  t1=Dates.datetime2unix(Dates.DateTime(2016,10,1)) # was 10
  b=calculate_baseline(cc,24*3600)
  indc=(cc.time .>= t0) .& (cc.time .<= t1)
  
  # read rain data
  drain=Feather.read(tel4rain.rainfile) # our estimation
  dl=DataFrame(CSV.File("../dry_wet_classification_Letnany.csv";delim=';'))
  indr=(dl.time .>= t0) .& (dl.time .<= t1)
  indd=(drain.intn .>= tel4rain.time_to_intnum(t0) ) .& (drain.intn .<= tel4rain.time_to_intnum(t1))
  plt1=Plots.plot(cc.time[indc],[cc.a[indc],b[indc]],ylabel="dB",xlabel="time",label=["attn","baseline"])
  plt2=Plots.plot(dl.time[indr],dl.wet_prob[indr],ylabel="probability",xlabel="time",label="wetprob")
  plt3=Plots.plot(drain.intn[indd]*tel4rain.interval,drain.rain[indd],ylabel="rain",xlabel="time")

  l=@layout [a;b;c] ; plt=Plots.plot(plt1,plt2,plt3,layout=l)
  return plt
end

function show_two_signals()
  f=open("../links.jsl","r") # read already parsed link data
  @time l2=deserialize(f)
  c1=l2[56]                  # choose channel id
  c2=l2[57]                  # choose channel id
  #t0=Dates.datetime2unix(Dates.DateTime(2016,7,5)) # was 1
  #t1=Dates.datetime2unix(Dates.DateTime(2016,7,6)) # was 10
  t0=Dates.datetime2unix(Dates.DateTime(2016,9,16)) # was 1
  #t1=Dates.datetime2unix(Dates.DateTime(2016,9,17))+24*3600 # was 10
  t1=Dates.datetime2unix(Dates.DateTime(2016,9,18)) # was 10
  b1=calculate_baseline(c1,24*3600)
  b2=calculate_baseline(c2,24*3600)
  indc1=(c1.time .>= t0) .& (c1.time .<= t1)
  indc2=(c2.time .>= t0) .& (c2.time .<= t1)
  
  # read rain data
  drain=Feather.read(tel4rain.rainfile) # our estimation
  #dl=DataFrame(CSVFiles.load("../dry_wet_classification_Letnany.csv";delim=';'))
  #indr=(dl.time .>= t0) .& (dl.time .<= t1)
  indd=(drain.intn .>= tel4rain.time_to_intnum(t0) ) .& (drain.intn .<= tel4rain.time_to_intnum(t1))
  plt1=Plots.plot(c1.time[indc1],c1.a[indc1] .- b1[indc1],ylabel="dB",xlabel="time",label=["attn"])
  plt2=Plots.plot(c2.time[indc2],c2.a[indc2] .- b2[indc2],ylabel="dB",xlabel="time",label=["attn"])
  #  plt2=Plots.plot(c2.time[indc2],c2.a[indc2],ylabel="dB",xlabel="time",label=["attn"])
  plt3=Plots.scatter(drain.intn[indd]*tel4rain.interval,drain.rain[indd],marker=:color, markersize=1, ylabel="rain",xlabel="time")

  l=@layout [a;b;c] ; plt=Plots.plot(plt1,plt2,plt3,layout=l)
  return plt
end
        

function show_features() 
  f=open("../links.jsl","r") # read already parsed link data
  @time l2=deserialize(f)
  c1=l2[56]                  # choose channel id
  t0=Dates.datetime2unix(Dates.DateTime(2016,9,16)) # was 1
  #t1=Dates.datetime2unix(Dates.DateTime(2016,9,17))+24*3600 # was 10
  t1=Dates.datetime2unix(Dates.DateTime(2016,9,18)) # was 10
  xx=deserialize(open("../features.jsl","r"))
  xx=xx[xx.channel .== 56,:]
  xx=xx[t0 .<= xx.time .<= t1,:]
  y=xx.y
  indc1=(c1.time .>= t0) .& (c1.time .<= t1)
  drain=Feather.read(tel4rain.rainfile) # our estimation
  indd=(drain.intn .>= tel4rain.time_to_intnum(t0) ) .& (drain.intn .<= tel4rain.time_to_intnum(t1))
  plt1=Plots.plot(c1.time[indc1],c1.a[indc1],ylabel="dB",xlabel="time",label=["56"])
  plt2=Plots.plot(xx.time,[xx.x1,xx.x2,xx.x3,xx.x4],ylabel="dB",xlabel="time")
  plt3=Plots.scatter(drain.intn[indd]*tel4rain.interval,drain.rain[indd],marker=:color, markersize=1, ylabel="rain",xlabel="time")
  l=@layout [a;b;c] ; plt=Plots.plot(plt1,plt2,plt3,layout=l)
  return plt
end
    
""" show just one attenuation signal. call like
    f=open("../links.jsl","r")
    @time l2=deserialize(f)
    tel4rain.show_signal(l2[56])
"""

function show_signal(cc)
  t0=Dates.datetime2unix(Dates.DateTime(2016,7,5)) # was 1
  t1=Dates.datetime2unix(Dates.DateTime(2016,7,6)) # was 10
  indc=(cc.time .>= t0) .& (cc.time .<= t1)
  plt=Plots.plot(cc.time[indc],cc.a[indc],ylabel="dB",xlabel="time [s]")
  return plt
end

function calc_maximuml()
  f=open("../links.jsl","r")
  @time l2=deserialize(f)
  t0=Dates.datetime2unix(Dates.DateTime(2016,7,5)) # was 1
  t1=Dates.datetime2unix(Dates.DateTime(2016,7,6)) # was 10
  for k in keys(l2)
        cc=l2[k]
        b=calculate_baseline(cc,24*3600)
        indc=(cc.time .>= t0) .& (cc.time .<= t1)
        if sum(indc)>0
            println(k,", ",maximum(cc.a[indc] .- b[indc]))
        end
  end
end

function save_features()
  l=deserialize(open("../links.jsl","r"))
  drain=Feather.read(tel4rain.rainfile)
  f=tel4rain.create_features(l,drain,channels=[56,57])
  open("../features.jsl","w") do fd
    @time serialize(fd,f)
    close(fd)
  end
end

end # module
