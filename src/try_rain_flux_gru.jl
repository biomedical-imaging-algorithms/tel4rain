"""
# Dry/rain classification using Flux

Jan Kybic
"""

#import Pkg
#Pkg.activate(".")

using Markdown
using InteractiveUtils
using Serialization
using Flux.Data: DataLoader
using Flux
using Base.Iterators: partition, repeated, take
using Statistics: mean
using TimerOutputs: @timeit, TimerOutput
using ProgressMeter: @showprogress
using Plots
using Parameters
import Dates
import Feather
import Missings
import Random
import BSON
import CUDA
import ThreadsX

include("tel4rain.jl")



Random.seed!(12322) # initialize Random generator
#plotly() # initialize Plot backend

md"""Let us read the attenuation of one of the radio channels in one minute intervals for a defined time interval."""
t0=Dates.datetime2unix(Dates.DateTime(2016,6,1)) # initial date
t1=Dates.datetime2unix(Dates.DateTime(2016,10,1)) # final date
id=56 # channel number 

md"""The signals have been preprocessed by `process_all_links`. Let us read one of them. Read the rain information, convert to the same time scale and labels 1 (rains), -1 (dry), 0 (unknown)"""


""" From preprocessed data, extract channel `id` in interval between times `t0` and `t1`, resampled to one minute intervals and add rain information from the gauges"""
function getdata(t0,t1,id)
  @info "Reading data"  
  f=open("../links.jsl","r") # read already parsed link data
  l2=deserialize(f)
  cc=l2[id]
  indc=(cc.time .>= t0) .& (cc.time .<= t1)
  c=cc[indc,:]
  drain=Feather.read(tel4rain.rainfile)
  indd=(drain.intn .>= tel4rain.time_to_intnum(t0) ) .& (drain.intn .<= tel4rain.time_to_intnum(t1))
  drain=drain[indd,:]
  c.y=map(t -> convert(Int8,tel4rain.rainsz(drain,t)),c.time)
  return c	
end

traindata=getdata(t0,t1,id)

#md""" Here is the input attenuation signal """
#plot(traindata.time,traindata.a,title="Attenuation")
#plot(traindata.time,traindata.y,title="Rain classification")

testdata=getdata(Dates.datetime2unix(Dates.DateTime(2015,6,1)),
	Dates.datetime2unix(Dates.DateTime(2015,10,1)),id)

""" Structure Args contains all parameters """
@with_kw mutable struct Args
    lr::Float64 = 1e-3
    epochs::Int = 1000
    trainratio::Float64=0.8 # how much of the data to be used for training
    seed::Int = 100
    usecuda = false
    batchsize = 100
    infotime=1
    lambda=0.
    weight=10. # how much more weight to give to rainy periods    
    nstates=10 # state-space dimension
    maxnoimprovement=5 
    beta=1.0 # ratio of negative to positive samples
    #chunklen=2*24*60 # we will learn on sequences of this length (2 days)
    chunklen=4*60 # we will learn on sequences of this length (4 hours)
    savepath="runs/"
    #loadmodel=nothing
    loadmodel="runs/model20210420.bson" # or nothing if learning should start from scratch
    parallel=true
end


""" Given a vector of rain labels, return indices suitable for learning which will be interpreted as end indices of `chunklen` long intervals. The dry periods (label -1) will be subsampled so that their number is approximately `beta`*number of dry labels.""" 
function getinds(y;args)
	indsrain=filter(t -> t >= args.chunklen, findall(y .== 1))
	indsdry=filter(t -> t >= args.chunklen, findall(y .== -1))
	ndry=length(indsdry)
	nrain=length(indsrain)
	ndry=min(ndry,round(Int,args.beta*nrain))
	indsdry=Random.shuffle(indsdry)[1:ndry]
	return Random.shuffle(vcat(indsdry,indsrain))
end
	
md"""Generate sequences of training and testing data. We get  a sequence of batches. 
     Each batch itself is a sequence of pairs (x,y). `x` is a sequence of length `n` time points,
     each timepoint is a matrix `1 x m`, where `m` is the minibatch size. 
     `y` is a sequence of vectors of `m` labels (-1,0,1), where `0` means "don't care" """

function make_loader(data,inds,args=Args())
    # sequence of subarrays ending at indices `inds`
    X=( data.a[i-args.chunklen+1:i] for i in inds )
    Xbatched=( map(b -> reshape(b,(1,:)),Flux.batchseq(x,NaN32)) 
		         for x in partition(X,args.batchsize) )
    Y=( data.y[i-args.chunklen+1:i] for i in inds )
    Ybatched=( Flux.batchseq(y) for y in partition(Y,args.batchsize) )
    return zip(Xbatched,Ybatched)
end

""" evaluate logitcrossentropy for one time point of a batch given the softmax inputs
    `outpb` and the reference labels `yb` 
"""
function logitcrossentropy0(outpb,yb;weight=1.0)
    m=size(yb,1) # yb is a vector of labels
    @assert (size(outpb,1)==2) # matrix `2 x m`  of softmax inputs
    @assert (size(outpb,2)==m)
    epsilon=eps(Float32)
    logsumexp=log.(sum(exp.(outpb),dims=1).+epsilon)
    l=0.
    for j=1:m     # TODO parallelize if possible
      if yb[j]==-1
         l -= outpb[1,j] - logsumexp[j] 
      elseif yb[j]==1
         l -= weight .* ( outpb[2,j] - logsumexp[j] )
      end
    end
    return l
end
	

""" Criterion function given a batch,  composed of `m` sequences,
    represented by a sequence of length `n` of an array of `(2,m)` softmax inputs `x`
    and a sequence of length `n` of vectors of `(m,)` labels (-1,0,1), evaluate the cross-entropy.
    Items with label `0` are ignored. Returns the sum of all time points and sequences in the ."""
function loss(outp,y;weight=1.0) 
	return sum(logitcrossentropy0.(outp,y,weight=weight)) # sum over time
end
           

# """ Evaluate the classification accuracy, inputs as in `loss`. Returns also the number of sequences = number of minibatches * sequence length """
# function numcorr0(x,y)
#     m=size(y,1) # chunk size
#     @assert (size(x,1)==2)
#     @assert (size(x,2)==m)	
# 	rx=Flux.onecold(x)
# 	return sum( ( rx .== ( y .+ 3 ) ./ 2 ) .& ( y .!= 0 ))
# end


""" Convert softmax inputs to classes -1 or 1 """
function outp2class(outp)
	assert(length(outp)==2)
	return ifelse(outp[1]>outp[2],-1,1)
end

""" Convert softmax inputs to classes 1 or 2 """
function outp2classind(outp)
	@assert(length(outp)==2)
	return ifelse(outp[1]>outp[2],1,2)
end

""" convert -1,1 class encoding to 1,2 """
function classind(x)
	return ifelse(x>0,2,1)
end

""" Let us evaluate all possible binary classification outcomes by calculating the confusion matrix given one minibatch outputs and reference classification (-1,1)"""
function confmat0(outp,y)
  m=size(y,1)	
  @assert (size(outp,1)==2)
  @assert (size(outp,2)==m)	
  cm=zeros(Int,(2,2)) # confussion matrix
  for i=1:m
      if y[i]!=0
	  cm[classind(y[i]),outp2classind(outp[:,i])]+=1			
      end
  end # for i 
  return cm	
end

""" Calculate a confusion matrix given a sequence of minibatches """
function confmat(outps,ys)
	return sum(confmat0.(outps,ys))
end

#function numcorr(outp,y)
#	return sum(numcorr0.(outp,y))
#end

round4(x) = round(x, digits=4)


""" Do model evaluation on a dataset, report a number of measures. """
function eval_loss_accuracy(loader, model, device,args=Args())
    l = 0. # loss function
    cm=zeros(Int,(2,2)) # confusion matrix
    
    for (x, y) in loader # loop over all batches
        x, y = x |> device, y |> device
	Flux.reset!(model) # reset the sequence model
        outp = model.(x) # apply the model 
	l+= loss(outp,y,weight=args.weight) # 
	cm += confmat(outp |> cpu, y |> cpu)
	end
        ntot=sum(cm)
        corrs=cm[2,2]+cm[1,1]           
	return (loss = l/ntot |> round4, acc = corrs/ntot*100 |> round4,
		    tp=cm[2,2], fp=cm[1,2], tn=cm[1,1], fn=cm[2,1],
	        recall = cm[2,2]/(cm[2,2]+cm[2,1])*100 |> round4,
	        precision = cm[2,2]/(cm[2,2]+cm[1,2])*100 |> round4 )
end
		

""" build a recurrent model with N dimensional internal state """
function build_model(N)
	return Chain(GRU(1,N),GRU(N,N),Dense(N,2))
end


num_params(model) = sum(length, Flux.params(model)) 
#args=Args()


""" This function is to be called at the beginning of each epoch and will 
    return loaders for training  and testing data """
function get_loaders(args)
    @info "Creating loaders"
    training_inds=getinds(traindata.y,args=args)
    testing_inds=getinds(testdata.y,args=args)
    train_loader=make_loader(traindata,training_inds;args=args)
    test_loader=make_loader(testdata,testing_inds;args=args)
    return train_loader, test_loader
end

"""Train the model"""
function train(; kws...)
#function train(args)
    args = Args(; kws...)
    to=TimerOutput()

    args.seed > 0 && Random.seed!(args.seed)
    usecuda = args.usecuda && CUDA.functional()
    
    if usecuda
        device = gpu
        @info "Training on GPU"
    else
        device = cpu
        @info "Training on CPU"
    end

    ## DATA
    train_loader, test_loader = get_data(args)
    #training_inds=getinds(traindata.y,args=args)
    #testing_inds=getinds(testdata.y,args=args)
    #train_loader=make_loader(traindata,training_inds,args)
    #test_loader=make_loader(testdata,testing_inds,args)

    @info "Dataset Rain: $(length(train_loader)) train and $(length(test_loader)) test batches. Sequence length $(length(first(first(train_loader))))"

    ## MODEL AND OPTIMIZER
    model = build_model(args.nstates) 
    if isnothing(args.loadmodel)
        model = build_model(args.nstates)
    else
        @info "Loading model from $(args.loadmodel)" 
        BSON.@load args.loadmodel model
    end
    @info "GRU model: $(num_params(model)) trainable params"    
    @info "batch size: $(args.batchsize)"
    model=model |> device    
    ps = Flux.params(model)  

    opt = ADAM(args.lr) 
    if args.lambda > 0 # add weight decay, equivalent to L2 regularization
        opt = Optimiser(opt, WeightDecay(args.λ))
    end
    
    
    function report(epoch)
        train = eval_loss_accuracy(train_loader, model, device)
        test = eval_loss_accuracy(test_loader, model, device)        
        println("Epoch: $epoch   Train: $(train)   Test: $(test)")
        return test.loss
    end
    
    ## TRAINING
    if args.parallel
        @info "Start parallel training"
    else
        @info "Start Training"
    end
    report(0)
    prevacc=0. # previous accuracy
    noimprovement=0 # no improvement for that many epochs    
    nthreads=Threads.nthreads() # number of threads

    """ evaluate the gradient """
    function eval_gradient(m,(x,y))
        x, y = x |> device, y |> device
        ps=Flux.params(m)
        Flux.gradient(ps) do
            ŷ = m.(x) # apply to a sequence
            loss(ŷ, y,weight=args.weight)
        end
    end


    
    @timeit to "training" for epoch in 1:args.epochs
         if args.parallel
             @showprogress for b in partition(train_loader,nthreads)
                 Flux.reset!(model)
                 (xy,rest)=Iterators.peel(b)
                 rest=collect(rest)
                 models= [ deepcopy(model) for i=2:nthreads ]
                 gs=eval_gradient(model,xy)
                 gss=Array{typeof(gs)}(undef,nthreads-1)
                 Threads.@threads for i=1:nthreads-1
                     gss[i]=eval_gradient(models[i],rest[i])
                 end
                 ps=Flux.params(model)
                 for j=1:length(ps)
                     for i=2:nthreads
                         psi=Flux.params(gss[i])
                         gs[ps[j]] .+= gss[i][psi[j]]
                     end
                     gs[x] ./= nthreads
                 end
                 Flux.Optimise.update!(opt, ps, gs)
             end # for b (iterate over batches)
         else    
            @showprogress for (x, y) in train_loader
                x, y = x |> device, y |> device
                #@info "started batch $(length(x)) $(size(first(x))) $(size(first(y)))"
                Flux.reset!(model)
                gs = Flux.gradient(ps) do
                    ŷ = model.(x)
                    loss(ŷ, y,weight=args.weight)
                end

                Flux.Optimise.update!(opt, ps, gs)
            end # for (x,y) in one epoch
         end # if args.parallel
 
        ## Printing and logging
        if (epoch % args.infotime == 0)
            acc=report(epoch)
            if acc<prevacc
                noimprovement=0 # reset the counter
            else
                noimprovement += args.infotime
                if noimprovement >= args.maxnoimprovement
                    @info "No improvement in $noimprovement iterations, interrupting."
                    break
                end
            end
            prevacc=acc
        end
    end # for epochs
    @info "Training finished."
    print(to)
    !ispath(args.savepath) && mkpath(args.savepath)
    modelpath = joinpath(args.savepath, "model.bson") 
    let model = cpu(model) #return model to cpu before serialization
       BSON.@save modelpath model 
    end
    @info "Model saved in \"$(modelpath)\""
end # function train

#train()
