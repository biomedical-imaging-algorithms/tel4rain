# This script is to be run on the cmpgrid-79 server

import Pkg
Pkg.activate(".")
using Revise
using Flux
using Tel4Rain
#using Logging
#args=Args(parallel=true,chunklen=240,batchsize=16,weight=4,nstates=64,beta=4.0,maxnoimprovement=10,loadmodel="runs/model20210504d.bson")
args=Args(parallel=true,chunklen=240,batchsize=24,weight=10,nstates=64,beta=2.0,maxnoimprovement=10,loadmodel="runs/model202106221406.bson",lr0=1e-3)
#train_loader_maker,test_loader_maker=get_small_data_loader_makers(args)
train_loader_maker,test_loader_maker=get_big_data_loader_makers(args)
#test_loader=test_loader_maker()
#m=train(train_loader_maker,test_loader_maker,build_model(args),cpu,args=args,loss_function=mcc_loss)
m=train(train_loader_maker,test_loader_maker,build_model(args),cpu,args=args,loss_function=ce_loss)
println("Training finished")

