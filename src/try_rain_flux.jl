### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ 0fd2c001-49a7-42f8-ad0f-5bff75626add
using Serialization

# ╔═╡ b1087487-a59f-41bc-96e2-4342ce7f080c
using Flux.Data: DataLoader

# ╔═╡ 89e668d8-c3b4-4e76-9ca5-e378d9655170
using Flux

# ╔═╡ 21775c13-ae5e-4f0e-9889-9b37021445f0
using Base.Iterators: partition, repeated, take

# ╔═╡ ff584c0c-28b8-4bab-a172-4d01465fef42
using Statistics: mean

# ╔═╡ 1646b0b5-3631-4612-a092-d50f6ead96f5
using TimerOutputs: @timeit, TimerOutput

# ╔═╡ 4284d55b-353a-4980-b720-70e4a73de75e
using ProgressMeter: @showprogress

# ╔═╡ a26523de-3b94-4700-a65a-12b9dfc9849e
using Plots

# ╔═╡ 761e86c5-7bee-497e-98a2-084296bab560
using Parameters

# ╔═╡ 81488087-2a52-4004-8006-624ea421cf2f
include("tel4rain.jl")

# ╔═╡ 8308e2b8-9d17-11eb-3578-c1edead1f44a
md"""
# Dry/rain classification using Flux
"""

# ╔═╡ 1b2835f2-8479-4243-b97d-0b8636f9d8aa
import Dates

# ╔═╡ 1a984f49-b779-4acf-8ae4-35f37d08bc11
import Feather

# ╔═╡ f3465166-7e86-4164-b790-29fbc2fb3342
import Missings

# ╔═╡ 924f1936-144c-4125-86ca-5a91e4fff8bd
import Random

# ╔═╡ dd38ff80-e3c6-46ec-82d8-b0d997e81e63
import BSON

# ╔═╡ fb9b987a-f68e-405c-81b8-93d4891f66a1
Random.seed!(12322)

# ╔═╡ 4c0f8755-57f8-46df-aba3-51b219add278
plotly()

# ╔═╡ 24e53171-a5a3-40ee-b675-56db0d6a0618
md"""Let us read the attenuation of one of the radio channels in one minute intervals for a defined time interval."""

# ╔═╡ b3cfd05d-d9ee-4f85-9fd8-eddba8d0960d
 t0=Dates.datetime2unix(Dates.DateTime(2016,6,1)) # initial date

# ╔═╡ 448e0fff-839f-48f9-b759-ae876f5e8710
t1=Dates.datetime2unix(Dates.DateTime(2016,10,1)) # final date

# ╔═╡ 3c61a17a-d292-4ad5-930b-c3ac383c13f8
id=56 # channel number 

# ╔═╡ 1c68d76c-43f8-4cb4-8c83-1248c4bfa74e
md"""The signals have been preprocessed by `process_all_links`. Let us read one of them. Read the rain information, convert to the same time scale and labels 1 (rains), -1 (dry), 0 (unknown)"""

# ╔═╡ b0a55853-a6af-4b1f-a814-cc39b775c979
""" From preprocessed data, extract channel `id` in interval between times `t0` and `t1`, resampled to one minute intervals and add rain information from the gauges"""
function getdata(t0,t1,id)
  f=open("../links.jsl","r") # read already parsed link data
  @time l2=deserialize(f)
  cc=l2[id]
  indc=(cc.time .>= t0) .& (cc.time .<= t1)
  c=cc[indc,:]
  drain=Feather.read(tel4rain.rainfile)
  indd=(drain.intn .>= tel4rain.time_to_intnum(t0) ) .& (drain.intn .<= tel4rain.time_to_intnum(t1))
  drain=drain[indd,:]
  c.y=map(t -> convert(Int8,tel4rain.rainsz(drain,t)),c.time)
  return c	
end

# ╔═╡ f4cff7a8-1695-44e8-b06f-dd0280500e1d
traindata=getdata(t0,t1,id)

# ╔═╡ 43ef1e33-fed7-4a0e-bd5d-0ab12e958cc4
md""" Here is the input attenuation signal """

# ╔═╡ c2ff0663-4157-4916-9dea-f61639e982a8
plot(traindata.time,traindata.a,title="Attenuation")

# ╔═╡ ca7d1e16-aae9-4486-9dde-c6cde3714745
plot(traindata.time,traindata.y,title="Rain classification")

# ╔═╡ 6ec14da1-2474-4d07-92e6-8f26ab517f02
testdata=getdata(Dates.datetime2unix(Dates.DateTime(2015,6,1)),
	Dates.datetime2unix(Dates.DateTime(2015,10,1)),id)

# ╔═╡ f76f60ff-a11a-4d11-888d-14148e63ea0a
plot(testdata.time,testdata.y,title="Rain classification")

# ╔═╡ 78f3274b-9e0d-4df8-86f1-b9048fa2dd9a
plot(testdata.time,testdata.a,title="Attenuation")

# ╔═╡ 32b70668-f396-45e7-9531-91e727f6c709
""" Given a vector of rain labels, return indices suitable for learning which will be interpreted as end indices of `chunklen` long intervals. The dry periods (label -1) will be subsampled so that their number is approximately `beta`*number of dry labels.""" 
function getinds(y;args)
	indsrain=filter(t -> t >= args.chunklen, findall(y .== 1))
	indsdry=filter(t -> t >= args.chunklen, findall(y .== -1))
	ndry=length(indsdry)
	nrain=length(indsrain)
	ndry=min(ndry,round(Int,args.beta*nrain))
	indsdry=Random.shuffle(indsdry)[1:ndry]
	return Random.shuffle(vcat(indsdry,indsrain))
end
	

# ╔═╡ 0d636ade-9439-4a91-b83c-1761a4e5de90
md"""Generate sequences of training and testing data"""

# ╔═╡ 5b3248f2-a257-4e64-a5e6-4bbf9a9b5f7b
function make_loader(data,inds,args)
    # sequence of subarrays ending at indices `inds`
    X=( data.a[i-args.chunklen+1:i] for i in inds )
    Xbatched=( map(b -> reshape(b,(1,:)),Flux.batchseq(x,NaN32)) 
		         for x in partition(X,args.batchsize) )
    Y=( data.y[i-args.chunklen+1:i] for i in inds )
    Ybatched=( Flux.batchseq(y) for y in partition(Y,args.batchsize) )
    return zip(Xbatched,Ybatched)
end

# ╔═╡ bc09bef5-f713-4c22-a356-dcda4e60393c
function logitcrossentropy0(outpb,yb)
	m=size(yb,1) # yb is a vector of labels
	@assert (size(outpb,1)==2) # matrix `2 x m`  of softmax inputs
    @assert (size(outpb,2)==m)   
	logsumexp=sum(exp.(outpb),dims=1)
	l=0.
    for j=1:m     # TODO parallelize 
      if yb[j]==-1
         l -= outpb[1,j] - logsumexp[j] 
      elseif yb[j]==1
         l -= outpb[2,j] - logsumexp[j] 
      end
	end
	return l
end
	

# ╔═╡ 50f4cffb-1cfa-4390-839d-5b3b4c45d2e7
""" Criterion function given a batch,  composed of `m` minibatches,  represented by a sequence of length `n` of an array of `(2,m)` softmax inputs `x`
    and a sequence of length `n` of vectors of `(m,)` labels (-1,0,1), evaluate the cross-entropy.
    Items with label `0` are ignored. Returns the mean over all batches."""
function loss(outp,y) 
	return sum(logitcrossentropy0.(outp,y)) # sum over time
end
           

# ╔═╡ 2d1d0254-b676-4584-9ee5-03182b4cb22c
""" Evaluate the classification accuracy, inputs as in `loss`. Returns also the number of sequences = number of minibatches * sequence length """
function numcorr0(x,y)
    m=size(y,1) # chunk size
    @assert (size(x,1)==2)
    @assert (size(x,2)==m)	
	rx=Flux.onecold(x)
	return sum( ( rx .== ( y .+ 3 ) ./ 2 ) .& ( y .!= 0 ))
end

# ╔═╡ 23516dde-8b01-49c9-bef8-834de48d158f
""" Convert softmax inputs to classes -1 or 1 """
function outp2class(outp)
	assert(length(outp)==2)
	return ifelse(outp[1]>outp[2],-1,1)
end

# ╔═╡ 27121187-ed5a-428f-8795-5a78c643f9b6
""" Convert softmax inputs to classes 1 or 2 """
function outp2classind(outp)
	@assert(length(outp)==2)
	return ifelse(outp[1]>outp[2],1,2)
end

# ╔═╡ ae3030b9-f6db-4836-a709-d61e70bc47c2
""" convert -1,1 class encoding to 1,2 """
function classind(x)
	return ifelse(x>0,2,1)
end

# ╔═╡ bfdccee0-6691-464e-a844-7e82ac838e83
""" Let us evaluate all possible binary classification outcomes by calculating the confusion matrix given one minibatch outputs and reference classification (-1,1)"""
function confmat0(outp,y)
  m=size(y,1)	
  @assert (size(outp,1)==2)
  @assert (size(outp,2)==m)	
  cm=zeros(Int,(2,2)) # confussion matrix
  for i=1:m
		if y[i]!=0
			cm[classind(y[i]),outp2classind(outp[:,i])]+=1			
		end
  end # for i 
  return cm	
end

# ╔═╡ 7858f6e5-d44d-464e-b71c-5003cba86b56
""" Calculate a confusion matrix given a sequence of minibatches """
function confmat(outps,ys)
	return sum(confmat0.(outps,ys))
end

# ╔═╡ 394498f5-3008-48d1-b4db-921ef8788f28
function numcorr(outp,y)
	return sum(numcorr0.(outp,y))
end

# ╔═╡ f4d2e33e-bc36-4303-bd66-9c08af67b2b6
round4(x) = round(x, digits=4)

# ╔═╡ c9e2515f-d5d3-438d-b4b8-ccf453000ba3
function eval_loss_accuracy(loader, model, device)
	l = 0.
    corrs = 0
    ntot = 0
	cm=zeros(Int,(2,2))
    for (x, y) in loader
        x, y = x |> device, y |> device
		Flux.reset!(model)
        outp = model.(x)
		l+= loss(outp,y)
		#c=numcorr(outp |> cpu, y |> cpu)
		cm += confmat(outp |> cpu, y |> cpu)
		corrs+= numcorr(outp |> cpu, y |> cpu);
		#ntot+=length(y)*length(first(y))
	end
	ntot=sum(cm)
	#@show corrs sum(cm[1,1]+cm[2,2])
	return (loss = l/ntot |> round4, acc = corrs/ntot*100 |> round4,
		    tp=cm[2,2], fp=cm[1,2], tn=cm[1,1], fn=cm[2,1],
	        recall = cm[2,2]/(cm[2,2]+cm[2,1])*100 |> round4,
	        precision = cm[2,2]/(cm[2,2]+cm[1,2])*100 |> round4 )
end
		

# ╔═╡ 3f326d97-97b2-4e66-9169-7f7061c63e1c
""" build a recurrent model with N dimensional internal state """
function build_model(N)
	return Chain(GRU(1,N),Dense(N,2))
end

# ╔═╡ 0b6ac44d-aaef-427f-89ca-2c8f2eee7188
model=build_model(3)

# ╔═╡ f9ff11ec-4366-454f-b707-74e40aa6a4ba
model[1].state

# ╔═╡ 3db637a6-a520-40f4-ace4-126a67ef251d


# ╔═╡ 08d07977-e10d-4764-9042-d4573499ca3d
Flux.reset!(model)

# ╔═╡ 2e38a630-29c6-4d08-8904-9ecd683fbb1d
num_params(model) = sum(length, Flux.params(model)) 

# ╔═╡ 0b287cc7-fba2-4d2e-8a63-afa4f340bfa1
num_params(model)

# ╔═╡ dd669317-6354-4aed-b0c1-db81e9f84567
@with_kw mutable struct Args
    lr::Float64 = 0.1
    epochs::Int = 1000
    trainratio::Float64=0.8 # how much of the data to be used for training
    seed::Int = 100
    usecuda = false
    batchsize = 10
    infotime=1
    lambda=0.
	nstates=10 # state-space dimension
    maxnoimprovement=5 
	beta=1.0 # ratio of negative to positive samples
	chunklen=2*24*60 # we will learn on sequences of this length (2 days)
end

# ╔═╡ e6a01c3b-c964-402f-8280-a683ddcb49c0
args=Args()

# ╔═╡ 73d3f30f-eff3-4f96-88ab-1b39a6c515b9
training_inds=getinds(traindata.y,args=args)

# ╔═╡ 61dff268-87b9-4efd-b895-3f459c310974
length(training_inds)

# ╔═╡ 685be813-8649-4df2-a801-486bfa257b95
#testing_inds=args.chunklen:size(testdata,1) # testing indices
testing_inds=getinds(testdata.y,args=args)

# ╔═╡ 08be1195-2dca-4633-b938-b86c70312f25
length(testing_inds)

# ╔═╡ 5e2a2f7b-f555-4635-ba1f-b7dcb4c5fe27
train_loader=make_loader(traindata,training_inds,args)

# ╔═╡ a588de53-1ddf-4fdc-8fbf-c322d5e29410
test_loader=make_loader(testdata,testing_inds,args)

# ╔═╡ 4bb3547f-afe5-44e5-b258-2a56bde5fd7a
(x,y)=first(test_loader)

# ╔═╡ 66c7e368-eeae-4f9c-b937-7eed029e6798
x1,y1=first(test_loader)

# ╔═╡ cd295350-947f-41d9-a101-b3c259c277ce
outp=model.(x1)

# ╔═╡ c9ddb690-6709-49bb-a461-eafdf6fa5062
 loss(outp,y1)

# ╔═╡ f94cb601-24bf-4e0f-9290-ccad6074f7d8
numcorr(outp,y1)

# ╔═╡ 8a219670-e4b3-4807-9b7e-87950502c2ce
eval_loss_accuracy(test_loader,model,cpu)

# ╔═╡ ae050385-1a90-48a2-85af-384e75f5090c
"""Train the model"""
function train(; kws...)
    args = Args(; kws...)
    to=TimerOutput()

    args.seed > 0 && Random.seed!(args.seed)
    usecuda = args.usecuda && CUDA.functional()
    
    if usecuda
        device = gpu
        @info "Training on GPU"
    else
        device = cpu
        @info "Training on CPU"
    end

    ## DATA
    #train_loader, test_loader = get_data(args)
    @info "Dataset Rain: $(length(train_loader)) train and $(length(test_loader)) test batches"

    ## MODEL AND OPTIMIZER
    model = build_model(args.nstates) |> device
    @info "GRU model: $(num_params(model)) trainable params"    

    @info "batch size: $(args.batchsize)"
    
    ps = Flux.params(model)  

    opt = ADAM(args.lr) 
    if args.lambda > 0 # add weight decay, equivalent to L2 regularization
        opt = Optimiser(opt, WeightDecay(args.λ))
    end
    
    
    function report(epoch)
        train = eval_loss_accuracy(train_loader, model, device)
        test = eval_loss_accuracy(test_loader, model, device)        
        println("Epoch: $epoch   Train: $(train)   Test: $(test)")
        return test.loss
    end
    
    ## TRAINING
    @info "Start Training"
    report(0)
    prevacc=0. # previous accuracy
    noimprovement=0 # no improvement for that many epochs    
    @timeit to "training" for epoch in 1:args.epochs
        @showprogress for (x, y) in train_loader
            x, y = x |> device, y |> device
            gs = Flux.gradient(ps) do
                    ŷ = model.(x)
                    loss(ŷ, y)
                end

            Flux.Optimise.update!(opt, ps, gs)
		end # for (x,y) in one epoch
        
        ## Printing and logging
        if (epoch % args.infotime == 0)
            acc=report(epoch)
            if acc>prevacc
                noimprovement=0 # reset the counter
            else
                noimprovement += args.infotime
                if noimprovement >= args.maxnoimprovement
                    @info "No improvement in $noimprovement iterations, interrupting."
                    break
                end
            end
            prevacc=acc
        end
    end # for epochs
    @info "Training finished."
    print(to)
    !ispath(args.savepath) && mkpath(args.savepath)
    modelpath = joinpath(args.savepath, "model.bson") 
    let model = cpu(model) #return model to cpu before serialization
       BSON.@save modelpath model 
    end
    @info "Model saved in \"$(modelpath)\""
end # function train


# ╔═╡ d0b62d66-e90d-4a0c-948c-d729e3870253
train()

# ╔═╡ Cell order:
# ╟─8308e2b8-9d17-11eb-3578-c1edead1f44a
# ╠═81488087-2a52-4004-8006-624ea421cf2f
# ╠═1b2835f2-8479-4243-b97d-0b8636f9d8aa
# ╠═0fd2c001-49a7-42f8-ad0f-5bff75626add
# ╠═1a984f49-b779-4acf-8ae4-35f37d08bc11
# ╠═f3465166-7e86-4164-b790-29fbc2fb3342
# ╠═924f1936-144c-4125-86ca-5a91e4fff8bd
# ╠═b1087487-a59f-41bc-96e2-4342ce7f080c
# ╠═89e668d8-c3b4-4e76-9ca5-e378d9655170
# ╠═21775c13-ae5e-4f0e-9889-9b37021445f0
# ╠═ff584c0c-28b8-4bab-a172-4d01465fef42
# ╠═1646b0b5-3631-4612-a092-d50f6ead96f5
# ╠═4284d55b-353a-4980-b720-70e4a73de75e
# ╠═dd38ff80-e3c6-46ec-82d8-b0d997e81e63
# ╠═fb9b987a-f68e-405c-81b8-93d4891f66a1
# ╠═a26523de-3b94-4700-a65a-12b9dfc9849e
# ╠═4c0f8755-57f8-46df-aba3-51b219add278
# ╟─24e53171-a5a3-40ee-b675-56db0d6a0618
# ╠═b3cfd05d-d9ee-4f85-9fd8-eddba8d0960d
# ╠═448e0fff-839f-48f9-b759-ae876f5e8710
# ╠═3c61a17a-d292-4ad5-930b-c3ac383c13f8
# ╟─1c68d76c-43f8-4cb4-8c83-1248c4bfa74e
# ╠═b0a55853-a6af-4b1f-a814-cc39b775c979
# ╠═f4cff7a8-1695-44e8-b06f-dd0280500e1d
# ╟─43ef1e33-fed7-4a0e-bd5d-0ab12e958cc4
# ╠═c2ff0663-4157-4916-9dea-f61639e982a8
# ╠═ca7d1e16-aae9-4486-9dde-c6cde3714745
# ╠═6ec14da1-2474-4d07-92e6-8f26ab517f02
# ╠═f76f60ff-a11a-4d11-888d-14148e63ea0a
# ╠═78f3274b-9e0d-4df8-86f1-b9048fa2dd9a
# ╠═32b70668-f396-45e7-9531-91e727f6c709
# ╠═73d3f30f-eff3-4f96-88ab-1b39a6c515b9
# ╠═61dff268-87b9-4efd-b895-3f459c310974
# ╠═685be813-8649-4df2-a801-486bfa257b95
# ╠═08be1195-2dca-4633-b938-b86c70312f25
# ╠═761e86c5-7bee-497e-98a2-084296bab560
# ╠═e6a01c3b-c964-402f-8280-a683ddcb49c0
# ╟─0d636ade-9439-4a91-b83c-1761a4e5de90
# ╠═5b3248f2-a257-4e64-a5e6-4bbf9a9b5f7b
# ╠═5e2a2f7b-f555-4635-ba1f-b7dcb4c5fe27
# ╠═a588de53-1ddf-4fdc-8fbf-c322d5e29410
# ╠═4bb3547f-afe5-44e5-b258-2a56bde5fd7a
# ╠═bc09bef5-f713-4c22-a356-dcda4e60393c
# ╠═50f4cffb-1cfa-4390-839d-5b3b4c45d2e7
# ╠═2d1d0254-b676-4584-9ee5-03182b4cb22c
# ╠═23516dde-8b01-49c9-bef8-834de48d158f
# ╠═27121187-ed5a-428f-8795-5a78c643f9b6
# ╠═ae3030b9-f6db-4836-a709-d61e70bc47c2
# ╠═bfdccee0-6691-464e-a844-7e82ac838e83
# ╠═7858f6e5-d44d-464e-b71c-5003cba86b56
# ╠═394498f5-3008-48d1-b4db-921ef8788f28
# ╠═f4d2e33e-bc36-4303-bd66-9c08af67b2b6
# ╠═c9e2515f-d5d3-438d-b4b8-ccf453000ba3
# ╠═66c7e368-eeae-4f9c-b937-7eed029e6798
# ╠═3f326d97-97b2-4e66-9169-7f7061c63e1c
# ╠═0b6ac44d-aaef-427f-89ca-2c8f2eee7188
# ╠═f9ff11ec-4366-454f-b707-74e40aa6a4ba
# ╠═3db637a6-a520-40f4-ace4-126a67ef251d
# ╠═08d07977-e10d-4764-9042-d4573499ca3d
# ╠═2e38a630-29c6-4d08-8904-9ecd683fbb1d
# ╠═0b287cc7-fba2-4d2e-8a63-afa4f340bfa1
# ╠═cd295350-947f-41d9-a101-b3c259c277ce
# ╠═c9ddb690-6709-49bb-a461-eafdf6fa5062
# ╠═f94cb601-24bf-4e0f-9290-ccad6074f7d8
# ╠═8a219670-e4b3-4807-9b7e-87950502c2ce
# ╠═ae050385-1a90-48a2-85af-384e75f5090c
# ╠═dd669317-6354-4aed-b0c1-db81e9f84567
# ╠═d0b62d66-e90d-4a0c-948c-d729e3870253
