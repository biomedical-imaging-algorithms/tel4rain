# Classification of the MNIST dataset with Flux
# based on https://github.com/FluxML/model-zoo/blob/master/vision/conv_mnist/conv_mnist.jl
#
# Jan Kybic

using Flux
using Flux.Data: DataLoader
using Flux.Optimise: Optimiser, WeightDecay
using Flux: onehotbatch, onecold
using Flux.Losses: logitcrossentropy
using Statistics, Random
using TimerOutputs
using CUDA
import BSON
import MLDatasets
using ProgressMeter: @showprogress

# LeNet5 "constructor". 
# The model can be adapted to any image size
# and any number of output classes.
function LeNet5(; imgsize=(28,28,1), nclasses=10) 
    out_conv_size = (imgsize[1]÷4 - 3, imgsize[2]÷4 - 3, 16)
    
    return Chain(
            Conv((5, 5), imgsize[end]=>6, relu),
            MaxPool((2, 2)),
            Conv((5, 5), 6=>16, relu),
            MaxPool((2, 2)),
            flatten,
            Dense(prod(out_conv_size), 120, relu), 
            Dense(120, 84, relu), 
            Dense(84, nclasses)
          )
end

function get_data(args)
    xtrain, ytrain = MLDatasets.MNIST.traindata(Float32)
    xtest, ytest = MLDatasets.MNIST.testdata(Float32)

    xtrain = reshape(xtrain, 28, 28, 1, :) # WHCN order
    xtest = reshape(xtest, 28, 28, 1, :)

    ytrain, ytest = onehotbatch(ytrain, 0:9), onehotbatch(ytest, 0:9)

    train_loader = DataLoader((xtrain, ytrain), batchsize=args.batchsize, shuffle=true)
    test_loader = DataLoader((xtest, ytest),  batchsize=args.batchsize)
    
    return train_loader, test_loader
end

loss(ŷ, y) = logitcrossentropy(ŷ, y)

function eval_loss_accuracy(loader, model, device)
    l = 0f0
    acc = 0
    ntot = 0
    for (x, y) in loader
        x, y = x |> device, y |> device
        ŷ = model(x)
        l += loss(ŷ, y) * size(x)[end]        
        acc += sum(onecold(ŷ |> cpu) .== onecold(y |> cpu))
        ntot += size(x)[end]
    end
    return (loss = l/ntot |> round4, acc = acc/ntot*100 |> round4)
end

num_params(model) = sum(length, Flux.params(model)) 
round4(x) = round(x, digits=4)

# arguments for the `train` function 
Base.@kwdef mutable struct Args
    lr = 3e-4             # learning rate
    lambda = 0                # L2 regularizer param, implemented as weight decay
    batchsize = 100      # batch size
    epochs = 10          # number of epochs
    seed = 0             # set seed > 0 for reproducibility
    use_cuda = false      # if true use cuda (if available)
    infotime = 1 	     # report every `infotime` epochs
    #checktime = 5        # Save the model every `checktime` epochs. Set to 0 for no checkpoints.
    #tblogger = true      # log training with tensorboard
    savepath = "runs/"    # results path
    maxnoimprovement=2    
end

function train(; kws...)
    args = Args(; kws...)
    to=TimerOutput()

    args.seed > 0 && Random.seed!(args.seed)
    use_cuda = args.use_cuda && CUDA.functional()
    
    if use_cuda
        device = gpu
        @info "Training on GPU"
    else
        device = cpu
        @info "Training on CPU"
    end

    ## DATA
    train_loader, test_loader = get_data(args)
    @info "Dataset MNIST: $(train_loader.nobs) train and $(test_loader.nobs) test examples"

    ## MODEL AND OPTIMIZER
    model = LeNet5() |> device
    @info "LeNet5 model: $(num_params(model)) trainable params"    

    @info "batch size: $(args.batchsize)"
    
    ps = Flux.params(model)  

    opt = ADAM(args.lr) 
    if args.lambda > 0 # add weight decay, equivalent to L2 regularization
        opt = Optimiser(opt, WeightDecay(args.λ))
    end
    
    ## LOGGING UTILITIES
    #if args.tblogger 
    #    tblogger = TBLogger(args.savepath, tb_overwrite)
    #    set_step_increment!(tblogger, 0) # 0 auto increment since we manually set_step!
    #    @info "TensorBoard logging at \"$(args.savepath)\""
    #end
    
    function report(epoch)
        train = eval_loss_accuracy(train_loader, model, device)
        test = eval_loss_accuracy(test_loader, model, device)        
        println("Epoch: $epoch   Train: $(train)   Test: $(test)")
        #if args.tblogger
        #    set_step!(tblogger, epoch)
        #    with_logger(tblogger) do
        #        @info "train" loss=train.loss  acc=train.acc
        #        @info "test"  loss=test.loss   acc=test.acc
        #    end
        #end
        return test.acc
    end
    
    ## TRAINING
    @info "Start Training"
    report(0)
    prevacc=0. # previous accuracy
    noimprovement=0 # no improvement for that many epochs    
    @timeit to "training" for epoch in 1:args.epochs
        @showprogress for (x, y) in train_loader
            x, y = x |> device, y |> device
            gs = Flux.gradient(ps) do
                    ŷ = model(x)
                    loss(ŷ, y)
                end

            Flux.Optimise.update!(opt, ps, gs)
        end # for (x,y)
        
        ## Printing and logging
        if (epoch % args.infotime == 0)
            acc=report(epoch)
            if acc>prevacc
                noimprovement=0 # reset the counter
            else
                noimprovement += args.infotime
                if noimprovement >= args.maxnoimprovement
                    @info "No improvement in $noimprovement iterations, interrupting."
                    break
                end
            end
            prevacc=acc
        end
    #gc(); CuArrays.clearpool()
    end # for epochs
    @info "Training finished."
    print(to)
    !ispath(args.savepath) && mkpath(args.savepath)
    modelpath = joinpath(args.savepath, "model.bson") 
    let model = cpu(model) #return model to cpu before serialization
       BSON.@save modelpath model 
    end
    @info "Model saved in \"$(modelpath)\""
end # function train

train()
