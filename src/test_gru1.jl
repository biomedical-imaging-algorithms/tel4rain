### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 2b0322d9-ed2f-4c24-b46a-2855be764802
import Pkg

# ╔═╡ ce476a34-7eb6-4479-ba81-a3f69f3f48d0
Pkg.activate(".")

# ╔═╡ 188bed04-5189-4370-9449-e25fd01ad367
using Revise

# ╔═╡ 68009d0f-88e8-4e26-8b0c-673ab10041d4
using Flux

# ╔═╡ e0be167a-b271-4165-9571-c671b61fff0a
using Transducers

# ╔═╡ 38a98db9-3935-4496-8047-4180c3b090ec
using Tel4Rain

# ╔═╡ c8ae43ee-09ef-454f-84c7-e1fd3d225325
import Random

# ╔═╡ 20dee1c1-da5f-4617-a8ca-e91940c4f6c0
import Dates

# ╔═╡ b5cd5118-c806-4de5-b625-1fa7593476bb
import BSON

# ╔═╡ a2172d93-b7ca-45f4-8462-b379c759fc5e
import Profile

# ╔═╡ 8b711b99-261b-4ed2-9d14-3fb1a1d83856
Random.seed!(12322) # initialize Random generator

# ╔═╡ 5fa9c3ba-253d-4631-ba83-c7656f99cf4f
args=Args(parallel=true,chunklen=240,checkpointtime=100,weight=2,beta=2.0,maxnoimprovement=3,burnin=10,loadmodel="runs/model20210504d.bson")

# ╔═╡ 436ab06a-1e17-4233-bf4d-0ec6ead08d87
train_loader_maker,test_loader_maker=get_small_data_loader_makers(args)

# ╔═╡ ac4bf93f-a5af-4d42-acaa-a449c3b77004
test_loader=test_loader_maker()

# ╔═╡ e18149e0-542f-4f12-a79b-1c86ad7a1e05
model=build_model(args)

# ╔═╡ 845530d6-795b-4cb0-abe4-270dfa60cd3c
m2=train(train_loader_maker,test_loader_maker,build_model(args),cpu,args=args)

# ╔═╡ d7052014-91e9-46e9-9493-8df0c9dc836b
#@Profile.profile m3=train(train_loader_maker,test_loader_maker,build_model(args),cpu,args=args)

# ╔═╡ 97f3bf0c-6ef8-4c18-9ebc-842eb8471a08
#Profile.print(noisefloor=2.0,format=:tree,mincount=1000,sortedby=:count)

# ╔═╡ 25524d9f-96f8-4505-a1a6-c0f30063655a
eval_loss_accuracy(test_loader, m2, cpu, Tel4Rain.ce_loss, Args(args,parallel=true))

# ╔═╡ 9b501be7-4fab-40a6-ae88-2a76aceeb1bf
args2=Args(parallel=true,chunklen=240,weight=20,beta=10.0,burnin=10,maxnoimprovement=100,loadmodel="runs/model20210504c.bson")

# ╔═╡ 274a2295-1275-4917-8050-d25c3dc21d96
m3=BSON.load(args2.loadmodel,@__MODULE__)[:model]

# ╔═╡ 1b1e0cd7-a5a6-4a6f-96fa-9a929511ec7c
train_loader_maker2,test_loader_maker2=get_small_data_loader_makers(args2)

# ╔═╡ e8ab8d7f-efde-4e3e-a9c5-451310a34007
test_loader2=test_loader_maker2()

# ╔═╡ 77e9f9b6-949e-4e44-bf2d-51a07f763962
eval_loss_accuracy(test_loader2, m3, cpu, Tel4Rain.ce_loss, args2)

# ╔═╡ b1b9f231-600c-490d-990b-061a598fc1d0


# ╔═╡ Cell order:
# ╠═2b0322d9-ed2f-4c24-b46a-2855be764802
# ╠═ce476a34-7eb6-4479-ba81-a3f69f3f48d0
# ╠═68009d0f-88e8-4e26-8b0c-673ab10041d4
# ╠═c8ae43ee-09ef-454f-84c7-e1fd3d225325
# ╠═20dee1c1-da5f-4617-a8ca-e91940c4f6c0
# ╠═188bed04-5189-4370-9449-e25fd01ad367
# ╠═e0be167a-b271-4165-9571-c671b61fff0a
# ╠═b5cd5118-c806-4de5-b625-1fa7593476bb
# ╠═38a98db9-3935-4496-8047-4180c3b090ec
# ╠═a2172d93-b7ca-45f4-8462-b379c759fc5e
# ╠═8b711b99-261b-4ed2-9d14-3fb1a1d83856
# ╠═5fa9c3ba-253d-4631-ba83-c7656f99cf4f
# ╠═436ab06a-1e17-4233-bf4d-0ec6ead08d87
# ╠═ac4bf93f-a5af-4d42-acaa-a449c3b77004
# ╠═e18149e0-542f-4f12-a79b-1c86ad7a1e05
# ╠═845530d6-795b-4cb0-abe4-270dfa60cd3c
# ╠═d7052014-91e9-46e9-9493-8df0c9dc836b
# ╠═97f3bf0c-6ef8-4c18-9ebc-842eb8471a08
# ╠═25524d9f-96f8-4505-a1a6-c0f30063655a
# ╠═9b501be7-4fab-40a6-ae88-2a76aceeb1bf
# ╠═274a2295-1275-4917-8050-d25c3dc21d96
# ╠═1b1e0cd7-a5a6-4a6f-96fa-9a929511ec7c
# ╠═e8ab8d7f-efde-4e3e-a9c5-451310a34007
# ╠═77e9f9b6-949e-4e44-bf2d-51a07f763962
# ╠═b1b9f231-600c-490d-990b-061a598fc1d0
